gnome-shell-extension-weather (0.0~git20210509.d714eb1-4) unstable; urgency=medium

  * Move Vcs repo from the debian group to the gnome-team/shell-extensions
    subgroup on Salsa. Thanks to Joerg Jaspert for his help!
    See https://salsa.debian.org/salsa/support/-/issues/294
  * Set Debian GNOME Maintainers as the Maintainer,
    and move myself to the list of Uploaders

 -- Anthony Fok <foka@debian.org>  Wed, 13 Apr 2022 08:59:40 -0600

gnome-shell-extension-weather (0.0~git20210509.d714eb1-3) unstable; urgency=medium

  * Declare compatibility with GNOME Shell 41 and 42.
    Thanks to Simon McVittie for the bug report, and Shawn Buckley
    for the merge request upstream. (Closes: #1008562)
  * Add "Rules-Requires-Root: no" to debian/control
  * Update debian/copyright with more people and updated years,
    and fix superfluous-file-pattern src/convenience.js Lintian warning

 -- Anthony Fok <foka@debian.org>  Sat, 02 Apr 2022 22:56:25 -0600

gnome-shell-extension-weather (0.0~git20210509.d714eb1-2) unstable; urgency=medium

  * Set debian/watch to track upstream "master" branch again
    which is now even with the "gnome40" branch
  * Declare compatibility with GNOME Shell 41 (Closes: #996076)

 -- Anthony Fok <foka@debian.org>  Tue, 19 Oct 2021 13:05:09 -0600

gnome-shell-extension-weather (0.0~git20210509.d714eb1-1) unstable; urgency=medium

  * New upstream version 0.0~git20210509.d714eb1 (gnome40 branch)
  * Set debian/watch to track upstream "gnome40" branch
  * Bump gnome-shell dependency to (>= 40) (Closes: #993202)
  * Fix upstream URL in debian/control and debian/copyright as the project
    was moved from GitHub to GitLab back in June 2018 (Closes: #990907)
  * Add dependency on ca-certificates to prevent SSL handshake failure
    when loading openweathermap data.  Thanks to Alexei Ustyuzhaninov
    for the report and Adrian Bunk for the fix (Closes: #990418)
  * Bump Standards-Version to 4.6.0 (no change)

 -- Anthony Fok <foka@debian.org>  Fri, 17 Sep 2021 23:19:51 -0600

gnome-shell-extension-weather (0.0~git20201103.d8be50f-1) unstable; urgency=medium

  * New maintainer (Closes: #828694)
  * Add debian/watch
  * New upstream version 0.0~git20201103.d8be50f
    (Closes: #912875, #955401, #956164, #969273)
  * debian/control:
    - Apply "cme fix dpkg" fixes:
      + Organize debian/control fields
      + Bump Standards-Version to 4.5.0 (no change)
    - Bump debhelper dependency to "Build-Depends: debhelper-compat (= 13)"
    - Bump gnome-shell dependency to (>= 3.38)
  * debian/rules:
    - Remove deprecated get-orig-source target
    - Remove override_dh_auto_test target as "make check" no longer crashes
  * Add debian/patches/fix-icons.patch:
    - Fix missing icons due to change in Adwaita icon pack 3.38, see
      https://gitlab.com/jenslody/gnome-shell-extension-openweather/-/issues/278
      Special thanks to Victor Vásquez and David Rees for the fix.

 -- Anthony Fok <foka@debian.org>  Mon, 09 Nov 2020 05:38:25 -0700

gnome-shell-extension-weather (0~20170402.git34506a6-2) unstable; urgency=medium

  * QA upload
  * Update Vcs fields for migration to https://salsa.debian.org
  * Add basic gbp.conf
  * Build-Depend on debhelper-compat 12 and drop debian/compat
  * Recommend gnome-tweaks instead of transitional gnome-tweak-tool
    (Closes: #917786)

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 30 Dec 2018 08:17:37 -0500

gnome-shell-extension-weather (0~20170402.git34506a6-1) unstable; urgency=medium

  * QA upload
  * New upstream snapshot.
    - Fix installation detection by extensions.gnome.org (Closes: #859971)
    - Adjust the supported versions (>= 3.14, << 3.25)
    - Adjust the long description to match the new supported services
  * debian/control: Bump Standards-Version to 3.9.8 (no further changes)
  * debian/rules: Set the GIT_VERSION so the package version is displayed in
    the about dialog
  * debian/control: Drop the upper limit for the gnome-shell version. Nowadays
    gnome-shell API should be stable enough and it is not checking the
    supported version anyway.

 -- Laurent Bigonville <bigon@debian.org>  Sun, 09 Apr 2017 23:59:23 +0200

gnome-shell-extension-weather (0~20160325.gitb5415ec-2) unstable; urgency=medium

  * QA upload
  * Orphan package, see #828694
  * Add debian/patches/support-gnome-shell-322.patch:
    - Add 3.22 Beta and 3.22 final release version numbers to metadata.json
  * debian/control:
    - Bump gnome-shell dependency to (<< 3.23)

 -- Jeremy Bicha <jbicha@ubuntu.com>  Wed, 07 Sep 2016 11:11:28 -0400

gnome-shell-extension-weather (0~20160325.gitb5415ec-1) unstable; urgency=medium

  * New upstream snapshot.
  * d/copyright: reflect upstream changes.
  * d/control:
    + This release is compatible with GNOME 3.20.
    + Bump Standards-Version to 3.9.7, no changes needed.

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 27 Mar 2016 20:40:14 +0200

gnome-shell-extension-weather (0~20151125.gitccaa1eb-1) unstable; urgency=medium

  * New upstream snapshot.
    + No more UI freeze when network is brought up. (Closes: #807313)
  * d/copyright: reflect upstream changes.

 -- Sébastien Villemot <sebastien@debian.org>  Wed, 09 Dec 2015 21:55:24 +0100

gnome-shell-extension-weather (0~20151023.git34aa242-1) unstable; urgency=medium

  * New upstream snapshot.
    + Now warns about missing API key. (Closes: #801979)

 -- Sébastien Villemot <sebastien@debian.org>  Fri, 23 Oct 2015 23:12:37 +0200

gnome-shell-extension-weather (0~20151003.git339ec8a-1) unstable; urgency=medium

  * New upstream snapshot.
  * d/control: this release is compatible with GNOME 3.18.
  * d/copyright: reflect upstream changes.
  * d/NEWS: fix urgency of latest entry to make lintian happy.

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 03 Oct 2015 11:58:49 +0200

gnome-shell-extension-weather (0~20150615.git0162cf7-1) unstable; urgency=medium

  * New upstream snapshot.
    Compatible with GNOME Shell 3.16. (Closes: #788789)

 -- Sébastien Villemot <sebastien@debian.org>  Mon, 15 Jun 2015 22:49:47 +0200

gnome-shell-extension-weather (0~20140924.git7e28508-1) unstable; urgency=medium

  * New upstream snapshot.
    + Compatible with GNOME Shell 3.14.
  * Bump Standards-Version to 3.9.6, no changes needed.

 -- Sébastien Villemot <sebastien@debian.org>  Wed, 24 Sep 2014 20:18:40 +0200

gnome-shell-extension-weather (0~20140724.gitcad3b38-1) unstable; urgency=medium

  * New upstream snapshot.

 -- Sébastien Villemot <sebastien@debian.org>  Mon, 04 Aug 2014 22:49:30 +0200

gnome-shell-extension-weather (0~20140608.gitb830562-1) unstable; urgency=medium

  * New upstream snapshot.

 -- Sébastien Villemot <sebastien@debian.org>  Sat, 05 Jul 2014 11:20:45 +0200

gnome-shell-extension-weather (0~20140506.gitbad1a58-1) unstable; urgency=low

  * New upstream release.
    + Now uses openweather as a data source.
    + As a consequence, upstream renamed the extension to g-s-e-openweather.
    + Compatible with GNOME Shell 3.12.
    + Italian translation updated. (Closes: #735113)
  * Document these changes in NEWS.Debian.
  * Remove patches:
    + name-in-ext-prefs.patch (useless)
    + french.patch (applied upstream)
  * Bump Standards-Version to 3.9.5.

 -- Sébastien Villemot <sebastien@debian.org>  Thu, 08 May 2014 17:31:08 +0200

gnome-shell-extension-weather (0~20131021.git1bf555c-1) unstable; urgency=low

  * Switch to a new upstream. The move to libgweather was making the
    extension useless to people not living in a major city. The upstream
    which we now track is a fork of the previous upstream, and it uses
    Yahoo! Weather as data source.
    As a consequence, upgrades from versions <= 0~20130619.gitf74de79-2 no
    longer erase city settings. (Closes: #726977)
  * New patches:
    + name-in-ext-prefs.patch
    + french.patch

 -- Sébastien Villemot <sebastien@debian.org>  Thu, 24 Oct 2013 16:48:46 +0200

gnome-shell-extension-weather (0~20131020.git30174f2-2) unstable; urgency=low

  * Really include NEWS.Debian

 -- Sébastien Villemot <sebastien@debian.org>  Mon, 21 Oct 2013 10:25:56 +0200

gnome-shell-extension-weather (0~20131020.git30174f2-1) unstable; urgency=low

  * New upstream release
    + Compatible with GNOME Shell 3.10. (Closes: #726914)
    + With the switch to libgweather, Moscow sunrise/sunset time should now be
      correct. (Closes: #708564)
  * debian/copyright: reflect upstream changes
  * Require GNOME Shell 3.8 or 3.10
  * Document the switch to libgweather in NEWS.Debian

 -- Sébastien Villemot <sebastien@debian.org>  Sun, 20 Oct 2013 16:36:42 +0200

gnome-shell-extension-weather (0~20130619.gitf74de79-2) unstable; urgency=low

  [ Jeremy Bicha ]
  * Depend on needed gir packages

 -- Sébastien Villemot <sebastien@debian.org>  Thu, 15 Aug 2013 13:41:21 +0200

gnome-shell-extension-weather (0~20130619.gitf74de79-1) unstable; urgency=low

  * New upstream snapshot.
    + Uses SSL for connecting to Yahoo. (Closes: #706622)

 -- Sébastien Villemot <sebastien@debian.org>  Wed, 19 Jun 2013 14:21:58 +0200

gnome-shell-extension-weather (0~20130328.git1452a2d-1) unstable; urgency=low

  * New upstream release. Supports GNOME Shell 3.8. (Closes: #704147)
  * debian/control: gnome-shell 3.8 now satisfies the dependency.

 -- Sébastien Villemot <sebastien@debian.org>  Thu, 28 Mar 2013 21:11:34 +0100

gnome-shell-extension-weather (0~20130224.gitfc0ac23-1) unstable; urgency=low

  * Initial release. (Closes: #648653)

 -- Sébastien Villemot <sebastien@debian.org>  Tue, 05 Mar 2013 22:06:48 +0100

# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Jens Lody
# This file is distributed under the same license as the gnome-shell-extension-openweather package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gnome-shell-extension-openweather 1.1\n"
"Report-Msgid-Bugs-To: https://gitlab.com/jenslody/gnome-shell-extension-"
"openweather/issues\n"
"POT-Creation-Date: 2021-05-09 10:46+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: src/extension.js:181
msgid "..."
msgstr ""

#: src/extension.js:360
msgid ""
"Openweathermap.org does not work without an api-key.\n"
"Either set the switch to use the extensions default key in the preferences "
"dialog to on or register at https://openweathermap.org/appid and paste your "
"personal key into the preferences dialog."
msgstr ""

#: src/extension.js:414
msgid ""
"Dark Sky does not work without an api-key.\n"
"Please register at https://darksky.net/dev/register and paste your personal "
"key into the preferences dialog."
msgstr ""

#: src/extension.js:519 src/extension.js:531
#, javascript-format
msgid "Can not connect to %s"
msgstr ""

#: src/extension.js:843 data/weather-settings.ui:300
msgid "Locations"
msgstr ""

#: src/extension.js:855
msgid "Reload Weather Information"
msgstr ""

#: src/extension.js:870
msgid "Weather data provided by:"
msgstr ""

#: src/extension.js:880
#, javascript-format
msgid "Can not open %s"
msgstr ""

#: src/extension.js:887
msgid "Weather Settings"
msgstr ""

#: src/extension.js:938 src/prefs.js:1059
msgid "Invalid city"
msgstr ""

#: src/extension.js:949
msgid "Invalid location! Please try to recreate it."
msgstr ""

#: src/extension.js:1000 data/weather-settings.ui:567
msgid "°F"
msgstr ""

#: src/extension.js:1002 data/weather-settings.ui:568
msgid "K"
msgstr ""

#: src/extension.js:1004 data/weather-settings.ui:569
msgid "°Ra"
msgstr ""

#: src/extension.js:1006 data/weather-settings.ui:570
msgid "°Ré"
msgstr ""

#: src/extension.js:1008 data/weather-settings.ui:571
msgid "°Rø"
msgstr ""

#: src/extension.js:1010 data/weather-settings.ui:572
msgid "°De"
msgstr ""

#: src/extension.js:1012 data/weather-settings.ui:573
msgid "°N"
msgstr ""

#: src/extension.js:1014 data/weather-settings.ui:566
msgid "°C"
msgstr ""

#: src/extension.js:1055
msgid "Calm"
msgstr ""

#: src/extension.js:1058
msgid "Light air"
msgstr ""

#: src/extension.js:1061
msgid "Light breeze"
msgstr ""

#: src/extension.js:1064
msgid "Gentle breeze"
msgstr ""

#: src/extension.js:1067
msgid "Moderate breeze"
msgstr ""

#: src/extension.js:1070
msgid "Fresh breeze"
msgstr ""

#: src/extension.js:1073
msgid "Strong breeze"
msgstr ""

#: src/extension.js:1076
msgid "Moderate gale"
msgstr ""

#: src/extension.js:1079
msgid "Fresh gale"
msgstr ""

#: src/extension.js:1082
msgid "Strong gale"
msgstr ""

#: src/extension.js:1085
msgid "Storm"
msgstr ""

#: src/extension.js:1088
msgid "Violent storm"
msgstr ""

#: src/extension.js:1091
msgid "Hurricane"
msgstr ""

#: src/extension.js:1095
msgid "Sunday"
msgstr ""

#: src/extension.js:1095
msgid "Monday"
msgstr ""

#: src/extension.js:1095
msgid "Tuesday"
msgstr ""

#: src/extension.js:1095
msgid "Wednesday"
msgstr ""

#: src/extension.js:1095
msgid "Thursday"
msgstr ""

#: src/extension.js:1095
msgid "Friday"
msgstr ""

#: src/extension.js:1095
msgid "Saturday"
msgstr ""

#: src/extension.js:1101
msgid "N"
msgstr ""

#: src/extension.js:1101
msgid "NE"
msgstr ""

#: src/extension.js:1101
msgid "E"
msgstr ""

#: src/extension.js:1101
msgid "SE"
msgstr ""

#: src/extension.js:1101
msgid "S"
msgstr ""

#: src/extension.js:1101
msgid "SW"
msgstr ""

#: src/extension.js:1101
msgid "W"
msgstr ""

#: src/extension.js:1101
msgid "NW"
msgstr ""

#: src/extension.js:1174 src/extension.js:1183 data/weather-settings.ui:600
msgid "hPa"
msgstr ""

#: src/extension.js:1178 data/weather-settings.ui:601
msgid "inHg"
msgstr ""

#: src/extension.js:1188 data/weather-settings.ui:602
msgid "bar"
msgstr ""

#: src/extension.js:1193 data/weather-settings.ui:603
msgid "Pa"
msgstr ""

#: src/extension.js:1198 data/weather-settings.ui:604
msgid "kPa"
msgstr ""

#: src/extension.js:1203 data/weather-settings.ui:605
msgid "atm"
msgstr ""

#: src/extension.js:1208 data/weather-settings.ui:606
msgid "at"
msgstr ""

#: src/extension.js:1213 data/weather-settings.ui:607
msgid "Torr"
msgstr ""

#: src/extension.js:1218 data/weather-settings.ui:608
msgid "psi"
msgstr ""

#: src/extension.js:1223 data/weather-settings.ui:609
msgid "mmHg"
msgstr ""

#: src/extension.js:1228 data/weather-settings.ui:610
msgid "mbar"
msgstr ""

#: src/extension.js:1272 data/weather-settings.ui:586
msgid "m/s"
msgstr ""

#: src/extension.js:1276 data/weather-settings.ui:585
msgid "mph"
msgstr ""

#: src/extension.js:1281 data/weather-settings.ui:584
msgid "km/h"
msgstr ""

#: src/extension.js:1290 data/weather-settings.ui:587
msgid "kn"
msgstr ""

#: src/extension.js:1295 data/weather-settings.ui:588
msgid "ft/s"
msgstr ""

#: src/extension.js:1389
msgid "Loading ..."
msgstr ""

#: src/extension.js:1393
msgid "Please wait"
msgstr ""

#: src/extension.js:1454
msgid "Cloudiness:"
msgstr ""

#: src/extension.js:1458
msgid "Humidity:"
msgstr ""

#: src/extension.js:1462
msgid "Pressure:"
msgstr ""

#: src/extension.js:1466
msgid "Wind:"
msgstr ""

#: src/darksky_net.js:159 src/darksky_net.js:291 src/openweathermap_org.js:352
#: src/openweathermap_org.js:479
msgid "Yesterday"
msgstr ""

#: src/darksky_net.js:162 src/darksky_net.js:294 src/openweathermap_org.js:354
#: src/openweathermap_org.js:481
#, javascript-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] ""
msgstr[1] ""

#: src/darksky_net.js:174 src/darksky_net.js:176 src/openweathermap_org.js:368
#: src/openweathermap_org.js:370
msgid ", "
msgstr ""

#: src/darksky_net.js:271 src/openweathermap_org.js:473
msgid "Today"
msgstr ""

#: src/darksky_net.js:287 src/openweathermap_org.js:475
msgid "Tomorrow"
msgstr ""

#: src/darksky_net.js:289 src/openweathermap_org.js:477
#, javascript-format
msgid "In %d day"
msgid_plural "In %d days"
msgstr[0] ""
msgstr[1] ""

#: src/openweathermap_org.js:183
msgid "Thunderstorm with light rain"
msgstr ""

#: src/openweathermap_org.js:185
msgid "Thunderstorm with rain"
msgstr ""

#: src/openweathermap_org.js:187
msgid "Thunderstorm with heavy rain"
msgstr ""

#: src/openweathermap_org.js:189
msgid "Light thunderstorm"
msgstr ""

#: src/openweathermap_org.js:191
msgid "Thunderstorm"
msgstr ""

#: src/openweathermap_org.js:193
msgid "Heavy thunderstorm"
msgstr ""

#: src/openweathermap_org.js:195
msgid "Ragged thunderstorm"
msgstr ""

#: src/openweathermap_org.js:197
msgid "Thunderstorm with light drizzle"
msgstr ""

#: src/openweathermap_org.js:199
msgid "Thunderstorm with drizzle"
msgstr ""

#: src/openweathermap_org.js:201
msgid "Thunderstorm with heavy drizzle"
msgstr ""

#: src/openweathermap_org.js:203
msgid "Light intensity drizzle"
msgstr ""

#: src/openweathermap_org.js:205
msgid "Drizzle"
msgstr ""

#: src/openweathermap_org.js:207
msgid "Heavy intensity drizzle"
msgstr ""

#: src/openweathermap_org.js:209
msgid "Light intensity drizzle rain"
msgstr ""

#: src/openweathermap_org.js:211
msgid "Drizzle rain"
msgstr ""

#: src/openweathermap_org.js:213
msgid "Heavy intensity drizzle rain"
msgstr ""

#: src/openweathermap_org.js:215
msgid "Shower rain and drizzle"
msgstr ""

#: src/openweathermap_org.js:217
msgid "Heavy shower rain and drizzle"
msgstr ""

#: src/openweathermap_org.js:219
msgid "Shower drizzle"
msgstr ""

#: src/openweathermap_org.js:221
msgid "Light rain"
msgstr ""

#: src/openweathermap_org.js:223
msgid "Moderate rain"
msgstr ""

#: src/openweathermap_org.js:225
msgid "Heavy intensity rain"
msgstr ""

#: src/openweathermap_org.js:227
msgid "Very heavy rain"
msgstr ""

#: src/openweathermap_org.js:229
msgid "Extreme rain"
msgstr ""

#: src/openweathermap_org.js:231
msgid "Freezing rain"
msgstr ""

#: src/openweathermap_org.js:233
msgid "Light intensity shower rain"
msgstr ""

#: src/openweathermap_org.js:235
msgid "Shower rain"
msgstr ""

#: src/openweathermap_org.js:237
msgid "Heavy intensity shower rain"
msgstr ""

#: src/openweathermap_org.js:239
msgid "Ragged shower rain"
msgstr ""

#: src/openweathermap_org.js:241
msgid "Light snow"
msgstr ""

#: src/openweathermap_org.js:243
msgid "Snow"
msgstr ""

#: src/openweathermap_org.js:245
msgid "Heavy snow"
msgstr ""

#: src/openweathermap_org.js:247
msgid "Sleet"
msgstr ""

#: src/openweathermap_org.js:249
msgid "Shower sleet"
msgstr ""

#: src/openweathermap_org.js:251
msgid "Light rain and snow"
msgstr ""

#: src/openweathermap_org.js:253
msgid "Rain and snow"
msgstr ""

#: src/openweathermap_org.js:255
msgid "Light shower snow"
msgstr ""

#: src/openweathermap_org.js:257
msgid "Shower snow"
msgstr ""

#: src/openweathermap_org.js:259
msgid "Heavy shower snow"
msgstr ""

#: src/openweathermap_org.js:261
msgid "Mist"
msgstr ""

#: src/openweathermap_org.js:263
msgid "Smoke"
msgstr ""

#: src/openweathermap_org.js:265
msgid "Haze"
msgstr ""

#: src/openweathermap_org.js:267
msgid "Sand/Dust Whirls"
msgstr ""

#: src/openweathermap_org.js:269
msgid "Fog"
msgstr ""

#: src/openweathermap_org.js:271
msgid "Sand"
msgstr ""

#: src/openweathermap_org.js:273
msgid "Dust"
msgstr ""

#: src/openweathermap_org.js:275
msgid "VOLCANIC ASH"
msgstr ""

#: src/openweathermap_org.js:277
msgid "SQUALLS"
msgstr ""

#: src/openweathermap_org.js:279
msgid "TORNADO"
msgstr ""

#: src/openweathermap_org.js:281
msgid "Sky is clear"
msgstr ""

#: src/openweathermap_org.js:283
msgid "Few clouds"
msgstr ""

#: src/openweathermap_org.js:285
msgid "Scattered clouds"
msgstr ""

#: src/openweathermap_org.js:287
msgid "Broken clouds"
msgstr ""

#: src/openweathermap_org.js:289
msgid "Overcast clouds"
msgstr ""

#: src/openweathermap_org.js:291
msgid "Not available"
msgstr ""

#: src/openweathermap_org.js:384
msgid "?"
msgstr ""

#: src/prefs.js:197
msgid "Searching ..."
msgstr ""

#: src/prefs.js:209 src/prefs.js:247 src/prefs.js:278 src/prefs.js:282
#, javascript-format
msgid "Invalid data when searching for \"%s\""
msgstr ""

#: src/prefs.js:214 src/prefs.js:254 src/prefs.js:285
#, javascript-format
msgid "\"%s\" not found"
msgstr ""

#: src/prefs.js:232
msgid "You need an AppKey to search on openmapquest."
msgstr ""

#: src/prefs.js:233
msgid "Please visit https://developer.mapquest.com/ ."
msgstr ""

#: src/prefs.js:248
msgid "Do you use a valid AppKey to search on openmapquest ?"
msgstr ""

#: src/prefs.js:249
msgid "If not, please visit https://developer.mapquest.com/ ."
msgstr ""

#: src/prefs.js:339
msgid "Location"
msgstr ""

#: src/prefs.js:350
msgid "Provider"
msgstr ""

#: src/prefs.js:360
msgid "Result"
msgstr ""

#: src/prefs.js:550
#, javascript-format
msgid "Remove %s ?"
msgstr ""

#: src/prefs.js:563
msgid "No"
msgstr ""

#: src/prefs.js:564
msgid "Yes"
msgstr ""

#: src/prefs.js:1094
msgid "default"
msgstr ""

#: data/weather-settings.ui:25
msgid "Edit name"
msgstr ""

#: data/weather-settings.ui:36 data/weather-settings.ui:52
#: data/weather-settings.ui:178
msgid "Clear entry"
msgstr ""

#: data/weather-settings.ui:43
msgid "Edit coordinates"
msgstr ""

#: data/weather-settings.ui:59 data/weather-settings.ui:195
msgid "Extensions default weather provider"
msgstr ""

#: data/weather-settings.ui:78 data/weather-settings.ui:214
msgid "Cancel"
msgstr ""

#: data/weather-settings.ui:88 data/weather-settings.ui:224
msgid "Save"
msgstr ""

#: data/weather-settings.ui:164
msgid "Search by location or coordinates"
msgstr ""

#: data/weather-settings.ui:179
msgid "e.g. Vaiaku, Tuvalu or -8.5211767,179.1976747"
msgstr ""

#: data/weather-settings.ui:184
msgid "Find"
msgstr ""

#: data/weather-settings.ui:318
msgid "Chose default weather provider"
msgstr ""

#: data/weather-settings.ui:329
msgid "Personal Api key from openweathermap.org"
msgstr ""

#: data/weather-settings.ui:372
msgid "Personal Api key from Dark Sky"
msgstr ""

#: data/weather-settings.ui:383
msgid "Refresh timeout for current weather [min]"
msgstr ""

#: data/weather-settings.ui:395
msgid "Refresh timeout for weather forecast [min]"
msgstr ""

#: data/weather-settings.ui:418
msgid ""
"Note: the forecast-timout is not used for Dark Sky, because they do not "
"provide seperate downloads for current weather and forecasts."
msgstr ""

#: data/weather-settings.ui:441
msgid "Use extensions api-key for openweathermap.org"
msgstr ""

#: data/weather-settings.ui:450
msgid ""
"Switch off, if you have your own api-key for openweathermap.org and put it "
"into the text-box below."
msgstr ""

#: data/weather-settings.ui:462
msgid "Weather provider"
msgstr ""

#: data/weather-settings.ui:478
msgid "Chose geolocation provider"
msgstr ""

#: data/weather-settings.ui:500
msgid "Personal AppKey from developer.mapquest.com"
msgstr ""

#: data/weather-settings.ui:522
msgid "Geolocation provider"
msgstr ""

#: data/weather-settings.ui:538
#: data/org.gnome.shell.extensions.openweather.gschema.xml:63
msgid "Temperature Unit"
msgstr ""

#: data/weather-settings.ui:547
msgid "Wind Speed Unit"
msgstr ""

#: data/weather-settings.ui:556
#: data/org.gnome.shell.extensions.openweather.gschema.xml:67
msgid "Pressure Unit"
msgstr ""

#: data/weather-settings.ui:589
msgid "Beaufort"
msgstr ""

#: data/weather-settings.ui:622
msgid "Units"
msgstr ""

#: data/weather-settings.ui:638
#: data/org.gnome.shell.extensions.openweather.gschema.xml:113
msgid "Position in Panel"
msgstr ""

#: data/weather-settings.ui:647
msgid "Position of menu-box [%] from 0 (left) to 100 (right)"
msgstr ""

#: data/weather-settings.ui:656
#: data/org.gnome.shell.extensions.openweather.gschema.xml:76
msgid "Wind Direction by Arrows"
msgstr ""

#: data/weather-settings.ui:665
#: data/org.gnome.shell.extensions.openweather.gschema.xml:89
msgid "Translate Conditions"
msgstr ""

#: data/weather-settings.ui:674
#: data/org.gnome.shell.extensions.openweather.gschema.xml:93
msgid "Symbolic Icons"
msgstr ""

#: data/weather-settings.ui:683
msgid "Text on buttons"
msgstr ""

#: data/weather-settings.ui:692
#: data/org.gnome.shell.extensions.openweather.gschema.xml:101
msgid "Temperature in Panel"
msgstr ""

#: data/weather-settings.ui:701
#: data/org.gnome.shell.extensions.openweather.gschema.xml:105
msgid "Conditions in Panel"
msgstr ""

#: data/weather-settings.ui:710
#: data/org.gnome.shell.extensions.openweather.gschema.xml:109
msgid "Conditions in Forecast"
msgstr ""

#: data/weather-settings.ui:719
msgid "Center forecast"
msgstr ""

#: data/weather-settings.ui:728
#: data/org.gnome.shell.extensions.openweather.gschema.xml:137
msgid "Number of days in forecast"
msgstr ""

#: data/weather-settings.ui:737
#: data/org.gnome.shell.extensions.openweather.gschema.xml:141
msgid "Maximal number of digits after the decimal point"
msgstr ""

#: data/weather-settings.ui:747
msgid "Center"
msgstr ""

#: data/weather-settings.ui:748
msgid "Right"
msgstr ""

#: data/weather-settings.ui:749
msgid "Left"
msgstr ""

#: data/weather-settings.ui:880
#: data/org.gnome.shell.extensions.openweather.gschema.xml:125
msgid "Maximal length of the location text"
msgstr ""

#: data/weather-settings.ui:902
msgid "Layout"
msgstr ""

#: data/weather-settings.ui:935
msgid "Version: "
msgstr ""

#: data/weather-settings.ui:941
msgid "unknown (self-build ?)"
msgstr ""

#: data/weather-settings.ui:949
msgid ""
"<span>Weather extension to display weather information from <a href="
"\"https://openweathermap.org/\">Openweathermap</a> or <a href=\"https://"
"darksky.net\">Dark Sky</a> for almost all locations in the world.</span>"
msgstr ""

#: data/weather-settings.ui:963
msgid "Maintained by"
msgstr ""

#: data/weather-settings.ui:976
msgid "Webpage"
msgstr ""

#: data/weather-settings.ui:986
msgid ""
"<span size=\"small\">This program comes with ABSOLUTELY NO WARRANTY.\n"
"See the <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html"
"\">GNU General Public License, version 2 or later</a> for details.</span>"
msgstr ""

#: data/weather-settings.ui:997
msgid "About"
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:55
msgid "Weather Provider"
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:59
msgid "Geolocation Provider"
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:71
msgid "Wind Speed Units"
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:72
msgid ""
"Choose the units used for wind speed. Allowed values are 'kph', 'mph', 'm/"
"s', 'knots', 'ft/s' or 'Beaufort'."
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:77
msgid "Choose whether to display wind direction through arrows or letters."
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:81
msgid "City to be displayed"
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:85
msgid "Actual City"
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:97
msgid "Use text on buttons in menu"
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:117
msgid "Horizontal position of menu-box."
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:121
msgid "Refresh interval (actual weather)"
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:129
msgid "Refresh interval (forecast)"
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:133
msgid "Center forecastbox."
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:145
msgid "Your personal API key from openweathermap.org"
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:149
msgid "Use the extensions default API key from openweathermap.org"
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:153
msgid "Your personal API key from Dark Sky"
msgstr ""

#: data/org.gnome.shell.extensions.openweather.gschema.xml:157
msgid "Your personal AppKey from developer.mapquest.com"
msgstr ""
